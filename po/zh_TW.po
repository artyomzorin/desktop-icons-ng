# Chinese (Taiwan) translation for desktop-icons.
# Copyright (C) 2018 desktop-icons's COPYRIGHT HOLDER
# This file is distributed under the same license as the desktop-icons package.
# Yi-Jyun Pan <pan93412@gmail.com>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: desktop-icons master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-18 22:06+0200\n"
"PO-Revision-Date: 2018-10-24 21:31+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese (Taiwan) <chinese-l10n@googlegroups.com>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2\n"

#: askConfirmPopup.js:36 askNamePopup.js:37 desktopIconsUtil.js:211
#: desktopManager.js:1481
msgid "Cancel"
msgstr "取消"

#: askConfirmPopup.js:37
msgid "Delete"
msgstr "刪除"

#: askNamePopup.js:36
msgid "OK"
msgstr "確定"

#: askRenamePopup.js:40
msgid "Folder name"
msgstr "資料夾名稱"

#: askRenamePopup.js:40
msgid "File name"
msgstr "檔案名稱"

#: askRenamePopup.js:47
msgid "Rename"
msgstr "重新命名"

#: desktopIconsUtil.js:80
msgid "Command not found"
msgstr ""

#: desktopIconsUtil.js:202
msgid "Do you want to run “{0}”, or display its contents?"
msgstr "要執行「{0}」，還是顯示它的內容？"

#: desktopIconsUtil.js:203
msgid "“{0}” is an executable text file."
msgstr "「{0}」是可執行的文字檔。"

#: desktopIconsUtil.js:207
#, fuzzy
msgid "Execute in a terminal"
msgstr "開啟終端器"

#: desktopIconsUtil.js:209
msgid "Show"
msgstr ""

#: desktopIconsUtil.js:213
msgid "Execute"
msgstr ""

#: desktopManager.js:135
msgid "Nautilus File Manager not found"
msgstr ""

#: desktopManager.js:136
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""

#: desktopManager.js:563
msgid "New Folder"
msgstr "新增資料夾"

#: desktopManager.js:567
msgid "New Document"
msgstr "新增文件"

#: desktopManager.js:572
msgid "Paste"
msgstr "貼上"

#: desktopManager.js:576
msgid "Undo"
msgstr "復原"

#: desktopManager.js:580
msgid "Redo"
msgstr "重做"

#: desktopManager.js:586
msgid "Select all"
msgstr "全部選取"

#: desktopManager.js:592
#, fuzzy
msgid "Show Desktop in Files"
msgstr "在《檔案》中開啟桌面"

#: desktopManager.js:596 fileItem.js:913
#, fuzzy
msgid "Open in Terminal"
msgstr "開啟終端器"

#: desktopManager.js:602
msgid "Change Background…"
msgstr "變更背景圖片…"

#: desktopManager.js:611
msgid "Display Settings"
msgstr "顯示設定"

#: desktopManager.js:618
msgid "Desktop Icons settings"
msgstr ""

#: desktopManager.js:629
msgid "Scripts"
msgstr "命令稿"

#: desktopManager.js:1148 desktopManager.js:1196
msgid "Error while deleting files"
msgstr ""

#: desktopManager.js:1222
msgid "Are you sure you want to permanently delete these items?"
msgstr ""

#: desktopManager.js:1223
msgid "If you delete an item, it will be permanently lost."
msgstr "如果直接刪除本項目，它會無法還原。"

#: desktopManager.js:1339
#, fuzzy
msgid "New folder"
msgstr "新增資料夾"

#: desktopManager.js:1414
msgid "Can not email a Directory"
msgstr ""

#: desktopManager.js:1415
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr ""

#: desktopManager.js:1479
msgid "Select Extract Destination"
msgstr ""

#: desktopManager.js:1482
msgid "Select"
msgstr "選擇"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:153
msgid "Home"
msgstr "家目錄"

#: fileItem.js:809
msgid "Open All..."
msgstr ""

#: fileItem.js:809
msgid "Open"
msgstr "開啟"

#: fileItem.js:816
msgid "Open All With Other Application..."
msgstr ""

#: fileItem.js:816
msgid "Open With Other Application"
msgstr ""

#: fileItem.js:820
msgid "Launch using Dedicated Graphics Card"
msgstr ""

#: fileItem.js:828
msgid "Cut"
msgstr "剪下"

#: fileItem.js:831
msgid "Copy"
msgstr "複製"

#: fileItem.js:835
msgid "Rename…"
msgstr "重新命名…"

#: fileItem.js:839
msgid "Move to Trash"
msgstr "移動到垃圾桶"

#: fileItem.js:843
msgid "Delete permanently"
msgstr "永久刪除"

#: fileItem.js:849
msgid "Don't Allow Launching"
msgstr ""

#: fileItem.js:849
msgid "Allow Launching"
msgstr ""

#: fileItem.js:856
#, fuzzy
msgid "Empty Trash"
msgstr "清空回收桶"

#: fileItem.js:863
msgid "Eject"
msgstr "退出"

#: fileItem.js:870
msgid "Unmount"
msgstr "卸載"

#: fileItem.js:885
msgid "Extract Here"
msgstr "在這裡解開"

#: fileItem.js:888
msgid "Extract To..."
msgstr "解開到…"

#: fileItem.js:893
msgid "Send to..."
msgstr "傳送到…"

#: fileItem.js:897
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] ""
msgstr[1] ""

#: fileItem.js:900
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] ""
msgstr[1] ""

#: fileItem.js:905
#, fuzzy
msgid "Common Properties"
msgstr "屬性"

#: fileItem.js:905
msgid "Properties"
msgstr "屬性"

#: fileItem.js:909
#, fuzzy
msgid "Show All in Files"
msgstr "在《檔案》中顯示"

#: fileItem.js:909
msgid "Show in Files"
msgstr "在《檔案》中顯示"

#: preferences.js:91
msgid "Settings"
msgstr "設定"

#: preferences.js:98
msgid "Size for the desktop icons"
msgstr "桌面圖示的大小"

#: preferences.js:98
msgid "Tiny"
msgstr ""

#: preferences.js:98
msgid "Small"
msgstr "小圖示"

#: preferences.js:98
msgid "Standard"
msgstr "標準大小圖示"

#: preferences.js:98
msgid "Large"
msgstr "大圖示"

#: preferences.js:99
msgid "Show the personal folder in the desktop"
msgstr "在桌面顯示個人資料夾"

#: preferences.js:100
msgid "Show the trash icon in the desktop"
msgstr "在桌面顯示垃圾桶圖示"

#: preferences.js:101 schemas/org.gnome.shell.extensions.ding.gschema.xml:38
#, fuzzy
msgid "Show external drives in the desktop"
msgstr "在桌面顯示個人資料夾"

#: preferences.js:102 schemas/org.gnome.shell.extensions.ding.gschema.xml:43
#, fuzzy
msgid "Show network drives in the desktop"
msgstr "在桌面顯示個人資料夾。"

#: preferences.js:105
#, fuzzy
msgid "New icons alignment"
msgstr "圖示大小"

#: preferences.js:106
msgid "Top-left corner"
msgstr ""

#: preferences.js:107
msgid "Top-right corner"
msgstr ""

#: preferences.js:108
msgid "Bottom-left corner"
msgstr ""

#: preferences.js:109
msgid "Bottom-right corner"
msgstr ""

#: preferences.js:111 schemas/org.gnome.shell.extensions.ding.gschema.xml:48
msgid "Add new drives to the opposite side of the screen"
msgstr ""

#: preferences.js:112
msgid "Highlight the drop place during Drag'n'Drop"
msgstr ""

#: preferences.js:116
msgid "Settings shared with Nautilus"
msgstr ""

#: preferences.js:122
msgid "Click type for open files"
msgstr ""

#: preferences.js:122
msgid "Single click"
msgstr ""

#: preferences.js:122
msgid "Double click"
msgstr ""

#: preferences.js:123
#, fuzzy
msgid "Show hidden files"
msgstr "在《檔案》中顯示"

#: preferences.js:124
msgid "Show a context menu item to delete permanently"
msgstr ""

#: preferences.js:129
msgid "Action to do when launching a program from the desktop"
msgstr ""

#: preferences.js:130
msgid "Display the content of the file"
msgstr ""

#: preferences.js:131
msgid "Launch the file"
msgstr ""

#: preferences.js:132
msgid "Ask what to do"
msgstr ""

#: preferences.js:138
msgid "Show image thumbnails"
msgstr ""

#: preferences.js:139
msgid "Never"
msgstr ""

#: preferences.js:140
msgid "Local files only"
msgstr ""

#: preferences.js:141
msgid "Always"
msgstr ""

#: prefs.js:37
msgid ""
"To configure Desktop Icons NG, do right-click in the desktop and choose the "
"last item: 'Desktop Icons settings'"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:18
msgid "Icon size"
msgstr "圖示大小"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:19
msgid "Set the size for the desktop icons."
msgstr "設定桌面圖示的大小。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:23
msgid "Show personal folder"
msgstr "顯示個人資料夾"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:24
msgid "Show the personal folder in the desktop."
msgstr "在桌面顯示個人資料夾。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:28
msgid "Show trash icon"
msgstr "顯示垃圾桶圖示"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:29
msgid "Show the trash icon in the desktop."
msgstr "在桌面顯示垃圾桶圖示。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:33
#, fuzzy
msgid "New icons start corner"
msgstr "圖示大小"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:34
msgid "Set the corner from where the icons will start to be placed."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:39
msgid "Show the disk drives connected to the computer."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:44
#, fuzzy
msgid "Show mounted network volumes in the desktop."
msgstr "在桌面顯示個人資料夾。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:49
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:53
msgid "Shows a rectangle in the destination place during DnD"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:54
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""

#, fuzzy
#~ msgid "Show external disk drives in the desktop"
#~ msgstr "在桌面顯示個人資料夾"

#, fuzzy
#~ msgid "Show the external drives"
#~ msgstr "在桌面顯示個人資料夾"

#, fuzzy
#~ msgid "Show network volumes"
#~ msgstr "在《檔案》中顯示"

#~ msgid "Huge"
#~ msgstr "巨大圖示"
